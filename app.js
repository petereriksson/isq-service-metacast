var metacast = require('./libs/metacast-client');
var camio = require('./libs/camio-server');
var config = require('./libs/config.js');

camio.onRundownUpdated = function(rundown) {
  try {
    metacast.send({'msg': 'load', data: rundown});
    console.log('sending to metacast');
    } catch(e) {
      console.log('cannot send to metacast', e)
    }
}

metacast.onConnected = function() {
  console.log('connected');
  metacast.send({'msg': 'status'});
}

console.log('connecting to metacast', config.host, config.port);
metacast.init('ws://' + config.host + ':' + config.port, function(msg) {
  console.log('message from metacast: ', msg);
});

