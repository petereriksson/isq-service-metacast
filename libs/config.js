var nconf = require('nconf');

var configFile = 'config.json';

exports.host;
exports.port;

nconf.env().file({file: configFile});
readSettings();

function readSettings() {
  exports.host = nconf.get('METACAST_HOST');
  exports.port = nconf.get('METACAST_PORT');
}

