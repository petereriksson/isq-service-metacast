var DOMParser = require('xmldom').DOMParser;
var xpath = require('xpath');
var fs = require('fs');

exports.parse = function(data) {
  var doc = new DOMParser().parseFromString(data);
  var root = doc.documentElement;
  var serial = root.getAttribute('serial-number');
  var select = xpath.useNamespaces({"bmml": "http://www.chyron.com/2002/bmml"});
  var n = select('//bmml:context/bmml:events', doc);
  var uuid = n[0].getAttribute('uuid');
  var moniker = n[0].getAttribute('moniker');

  var res = {};
  res.id = uuid;
  res.moniker = moniker;
  res.serial = serial;
  res.items = [];
  
  var nodes = select('//bmml:resource', doc);
  for (var i = 0; i < nodes.length; i++) {
    res.items.push(parsePlaylistItem(nodes[i], select));
  }
  
  return res;
}

function parsePlaylistItem(node, select) {
  var id = node.getAttribute('moniker');
  id = id.split('?')[0];
  var item = {id: id};
  var nodes = select('bmml:url/text()', node);
  if (nodes.length > 0) {
    var path = nodes[0].nodeValue;
    var arr = path.split('\\');
    arr.splice(0, 2);
    item.dataMsg = arr.join('/');
  }  
  return item;
}

/*
fs.readFile('rundown.xml', function(err, data) {
  exports.parse(data.toString());
});
*/


