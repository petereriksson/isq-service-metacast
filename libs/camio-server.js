var net = require("net");
var iconv = require('iconv-lite');
var bmmlParser = require('../libs/bmmlParser');

var PORT = 10542;

var TERM = '</bmml:bmml>';
var ACK_MSG = '<mos><mosID>*</mosID><roAck><roID>*</roID><roStatus>OK</roStatus></roAck></mos>';

var buf = '';
var newSent = false;

exports.onRundownUpdated = function() {}

function buildHelloMsg(ncsID, status) {
  var msg = '<mos><mosID>*</mosID><ncsID>' + ncsID + 
    '</ncsID><roAck><roID>*</roID><roStatus>' + status + 
    '</roStatus></roAck></mos>';
  return msg;  
}

net.createServer(function(sock) {
    sock.on('error', function(err) {  });
    sock.on('data', function(data) {
      buf += data.toString('utf-8');
      
      var hello = findAndRemoveHello(buf);
      buf = hello.buf;
      if (hello.found) { 
        console.log('camio says hello', hello.host);
        send(sock, buildHelloMsg(hello.host, newSent?'OK':'NEW'));
        newSent = true;
      }
      
      var arr = buf.split(TERM);
      for (var i = 0; i < arr.length - 1; i++) {
        parsePacket(arr[0] + TERM);
        send(sock, ACK_MSG);
        arr.splice(0, 1);
      }
      buf = arr[0];
    });
    
    sock.on('close', function(data) {
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
    
}).listen(PORT);

function send(sock, data) {
  console.log('sending', data);
  sock.write(getBigEndianUtf16(data));
}

function getBigEndianUtf16(str) {
  return iconv.encode(str, 'utf16-be');
}

function findAndRemoveHello(buf) {
  var start = buf.indexOf('<hello>');
  var end = buf.indexOf('</hello>');
  
  if ((start > -1) && (end > start)) {
    var host = buf.substring(start + 7, end);
    var sliced = buf.substring(0, start) + 
      buf.substring(end + '</hello>'.length, buf.length);
    return {buf: sliced, found: true, host: host}
  } else {
    return {buf: buf, found: false}
  }
}

function parsePacket(packet) {
  var loadObj = {msg: 'load', data: packet};
  var rundown = bmmlParser.parse(packet);
  exports.onRundownUpdated(rundown);
}