var WebSocketClient = require('websocket').client;
var reconnectDelay = 5000;
var _client = null;
var _connection = null;

var Client = function(uri, callback) {
  this.socket = new WebSocketClient({
    maxReceivedMessageSize: 128 * 1024 * 1024, 
    keepalive: true,
    keepaliveInterval: 5000, 
    dropConnectionOnKeepaliveTimeout: true, 
    keepaliveGracePeriod: 10000
  });
  
  this.doReconnect = true;
  this.status = 'not started';
  this.connected = false;
  this.callback = callback;
  this.connection = null;
  
  this.onConnected = function () {};
  
  this.disconnect = function() {
    this.doReconnect = false;
	this.connected = false;
  }
  
  this.connect = function(client) {
    if (!client) client = this;
    this.socket.on('connectFailed', function(error) {
  	  client.connected = false;
      client.status = error.toString();
	  client.reconnect(reconnectDelay);
	  console.log('connectFailed');
    });

    this.socket.on('connect', function (connection) {
      client.status = 'connected to ' + uri;
  	  client.connected = true;
      console.log('ws connected');
	  client.connection = connection;
	  _connection = connection;
	  exports.onConnected();
      connection.on('error', function (error) {
        console.log('ws client error', error);
      });

      connection.on('close', function () {
        console.log('ws client closed');
    	client.connected = false;
		client.status = 'closed';
		client.reconnect();
      });

      connection.on('message', function (message) {
        try {
          var msg = JSON.parse(message.utf8Data);
          if (msg) {
              callback(msg);        
          } else {
            console.log('Non-valid json on replication client ' + message);
          }
        } catch(e) {
          console.log('Could not parse json from Metacast', msg);
        }
      });
    });
    
	
	this.reconnect = function() {
	  if (!this.doReconnect) {
	    return;
	  }
      setTimeout(function() { client.socket.connect(uri); }, 5000);
	};
	console.log('connecting', uri);
    this.socket.connect(uri);
  }
}

exports.init = function(uri, callback) {
  _client = new Client(uri, callback);
  _client.connect();
}

exports.send = function(obj) {
  console.log('to metacast', JSON.stringify(obj));
  _connection.sendUTF(JSON.stringify(obj));
}

exports.onConnected = function() {};

function killWebSocket(bucket) {
  client.doReconnect = false;
  if (client.connection) {  
    client.connection.close();
  }
}
