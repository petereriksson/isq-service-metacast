var winston = require('winston');

errorAsJSON = function(e) {
  return {
    message: e.message,
    stack: e.stack
  }
}

var logger = new winston.Logger({
  transports: [
    new winston.transports.DailyRotateFile(
      {
        datePattern: '.dd.txt', 
        filename: global.appDir + '/logs/isq-service-metacast', 
        level: 'info',
        colorize: true,
        maxSize: 2 * 1024 * 1024,
        eol: '\r\n'
      }),
    new winston.transports.Console({
      level: 'silly',
      handleExceptions: true,
      json: false,
      colorize: true
    })]
});;

module.exports = logger;